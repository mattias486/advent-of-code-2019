package com.mattias.advent.y2019.dayX;

import org.junit.Test;

public class DayXTest {

    DayX dayX = new DayX();

    @Test
    public void testPart01() {
        dayX.part01();
    }

    @Test
    public void testPart02() {
        dayX.part02();
    }
}
