package com.mattias.advent.y2019.day04;

import java.util.stream.IntStream;

public class Day04 {

    public long part01() {
        return IntStream.range(367479, 893699)
                .filter(this::twoAdjacentDigitsAreTheSame)
                .filter(this::digitsNeverDecrease)
                .count();
    }

    public long part02() {
        return IntStream.range(367479, 893699)
                .filter(this::twoAdjacentDigitsAreTheSame)
                .filter(this::digitsNeverDecrease)
                .filter(this::matchingDigitsIsNotPartOfLargerDigitGroup)
                .count();
    }

    protected boolean twoAdjacentDigitsAreTheSame(int i) {
        if(getDigit6(i) == (getDigit5(i))) {
            return true;
        }
        if(getDigit5(i) == getDigit4(i)) {
            return true;
        }
        if(getDigit4(i) == getDigit3(i)) {
            return true;
        }
        if(getDigit3(i) == getDigit2(i)) {
            return true;
        }
        return getDigit2(i) == getDigit1(i);
    }

    private int getDigit1(int i) {
        return i / 100_000;
    }

    private int getDigit2(int i) {
        return (i%100_000)/10_000;
    }

    private int getDigit3(int i) {
        return (i%10_000)/1_000;
    }

    private int getDigit4(int i) {
        return (i%1_000)/100;
    }

    private int getDigit5(int i) {
        return (i%100)/10;
    }

    private int getDigit6(int i) {
        return i % 10;
    }

    protected boolean digitsNeverDecrease(int i) {
        if(getDigit6(i) < getDigit5(i)) {
            return false;
        }
        if(getDigit5(i) < getDigit4(i)) {
            return false;
        }
        if(getDigit4(i) < (getDigit3(i))) {
            return false;
        }
        if(getDigit3(i) < getDigit2(i)) {
            return false;
        }
        if (getDigit2(i) < getDigit1(i)) {
            return false;
        }
        return true;
    }

    protected boolean matchingDigitsIsNotPartOfLargerDigitGroup(int i) {
        if (getDigit2(i) == getDigit1(i) && getDigit3(i) != getDigit2(i)) {
            return true;
        }
        if (getDigit3(i) == getDigit2(i) && getDigit4(i) != getDigit3(i) && getDigit1(i) != getDigit3(i)) {
            return true;
        }
        if (getDigit4(i) == getDigit3(i) && getDigit5(i) != getDigit4(i) && getDigit2(i) != getDigit4(i)) {
            return true;
        }
        if (getDigit5(i) == getDigit4(i) && getDigit6(i) != getDigit5(i) && getDigit3(i) != getDigit5(i)) {
            return true;
        }
        if (getDigit6(i) == getDigit5(i) && getDigit5(i) != getDigit4(i)) {
            return true;
        }
        return false;
    }
}
