package com.mattias.advent.y2019.day10;

import com.mattias.advent.y2019.common.FileUtil;
import lombok.Data;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day10 {

    FileUtil fileUtil = new FileUtil();

    @Data
    public static class Planet {
        final int x;
        final int y;
    }

    public void part01() throws FileNotFoundException {
        List<String> fileContentRows = fileUtil.getFileContentAsStringList("day10input");
        Set<Planet> points = IntStream.range(0, fileContentRows.size())
                .boxed()
                .flatMap(y -> IntStream.range(0, fileContentRows.get(0).length())
                        .boxed()
                        .map(x -> new Planet(x, y)))
                        .filter(p -> '#' == fileContentRows.get(p.getY()).charAt(p.getX()))
                .collect(Collectors.toSet());

        Map<Planet, Integer> reachablePlanets = points.stream().
                collect(Collectors.toMap(point -> point,
                        point -> getNumberOfReachablePlanets(point, points)));

        Planet pointWithMostReachablePlanets = reachablePlanets.keySet().stream()
                .max(Comparator.comparing(reachablePlanets::get))
                .orElseThrow(RuntimeException::new);

        System.out.println("Planet with most reachable planets : " + pointWithMostReachablePlanets +
                ". Number of reachable : " + reachablePlanets.get(pointWithMostReachablePlanets));

        drawReachablePlanets(pointWithMostReachablePlanets, points);
    }

    private void drawReachablePlanets(Planet planet, Set<Planet> planets) {
        List<Planet> reachablePlanets = getReachablePlanets(planet, planets);
        int endX = planets.stream().mapToInt(Planet::getX).max().orElse(0) -
                planets.stream().mapToInt(Planet::getX).min().orElse(0);
        int endY = planets.stream().mapToInt(Planet::getY).max().orElse(0) -
                planets.stream().mapToInt(Planet::getY).min().orElse(0);
        char[][] points = new char[endY+1][endX+1];
        IntStream.range(0, endY+1)
                .forEach(y -> Arrays.fill(points[y],'.'));
        points[planet.getY()][planet.getX()] = '!';
        reachablePlanets
                .forEach(planetItem -> points[planetItem.getY()][planetItem.getX()] = 'X');
        IntStream.range(0, endY+1)
                .forEach(y -> System.out.println(new String(points[y])));
    }

    private int getNumberOfReachablePlanets(Planet planet, Set<Planet> planetsToReach) {
        return getReachablePlanets(planet, planetsToReach).size();
    }

    private List<Planet> getReachablePlanets(Planet planet, Set<Planet> planetsToReach) {
        return planetsToReach.stream()
                .filter(p -> !planet.equals(p))
                .filter(planetToReach -> isPlanetReachable(planet, planetToReach, planetsToReach))
                .collect(Collectors.toList());
    }

    public boolean isPlanetReachable(Planet planet, Planet planetToReach, Set<Planet> planetsToReach) {
        List<Planet> possibleBlockingPlanetPositions = getPossiblePlanets(planet, planetToReach);
        return isPlanetReachable(planetsToReach, possibleBlockingPlanetPositions);
    }

    private List<Planet> getPossiblePlanets(Planet planet, Planet planetToReach) {
        int diffX = planetToReach.getX() - planet.getX();
        int diffY = planetToReach.getY() - planet.getY();

        float diffRatio = (float)diffY / diffX;

        if(diffX == 0) {
            return IntStream.range(1, Math.abs(diffY))
                    .map(yDelta -> diffY > 0 ? yDelta : 0 - yDelta)
                    .mapToObj(yDelta -> new Planet(planet.getX(), planet.getY() + yDelta))
                    .collect(Collectors.toList());
        } else if(diffY == 0) {
            return IntStream.range(1, Math.abs(diffX))
                    .map(xDelta -> diffX > 0 ? xDelta : 0 - xDelta)
                    .mapToObj(xDelta -> new Planet(planet.getX() + xDelta, planet.getY()))
                    .collect(Collectors.toList());
        } else  if(diffX <= diffY) {
            return IntStream.range(1, Math.abs(diffX))
                    .map(xDelta -> diffX > 0 ? xDelta : 0 - xDelta)
                    .filter(xDelta -> isYAxisInteger(diffRatio, xDelta))
                    .mapToObj(xDelta -> getPlanetFromXDelta(planet, diffRatio, xDelta))
                    .collect(Collectors.toList());
        }
        return IntStream.range(1, Math.abs(diffY))
                    .map(yDelta -> diffY > 0 ? yDelta : 0 - yDelta)
                    .filter(yDelta -> isXDeltaInteger(diffRatio, yDelta))
                    .mapToObj(yDelta -> getPossiblePlanetFromYDelta(planet, diffRatio, yDelta))
                    .collect(Collectors.toList());
    }

    private boolean isPlanetReachable(Set<Planet> planetsToReach, List<Planet> possibleBlockingPlanetPositions) {
        Optional<Planet> planetInTheWay = possibleBlockingPlanetPositions.stream()
                .filter(planetsToReach::contains)
                .findFirst();
        return !planetInTheWay.isPresent();
    }

    private boolean isXDeltaInteger(float diffRatio, int yDelta) {
        return isCloseToZero(getXAxisDelta(diffRatio, yDelta));
    }

    private boolean isYAxisInteger(float diffRatio, int xDelta) {
        return isCloseToZero(getYAxisDelta(diffRatio, xDelta));
    }

    private boolean isCloseToZero(float delta) {
        return Math.abs(Math.round(delta) - delta) < 0.0002;
    }

    private Planet getPlanetFromXDelta(Planet planet, float diffRatio, int xDelta) {
        return new Planet(planet.getX() + xDelta,
                planet.getY() + Math.round(getYAxisDelta(diffRatio, xDelta)));
    }

    private Planet getPossiblePlanetFromYDelta(Planet planet, float diffRatio, int yDelta) {
        return new Planet(planet.getX() + Math.round(getXAxisDelta(diffRatio, yDelta)),
                planet.getY() + yDelta);
    }

    private float getYAxisDelta(float diffRatio, int xDelta) {
        return xDelta * diffRatio;
    }

    private float getXAxisDelta(float diffRatio, int yDelta) {
        return yDelta / diffRatio;
    }
}
