package com.mattias.advent.y2019.day08;

import com.mattias.advent.y2019.common.FileUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day08 {

    FileUtil fileUtil = new FileUtil();

    int pixelsWide = 25;
    int pixelsHigh = 6;
    int pixelsPerLayer = pixelsWide * pixelsHigh;

    public int part01() throws FileNotFoundException {
        List<String> layers = getLayers();
        Map<String, Integer> layersAndNumberOfZeros = layers.stream().collect(Collectors.toMap(
                i -> i,
                i -> StringUtils.countMatches(i, "0")
        ));
        int leastNumbersOfZeros = layersAndNumberOfZeros.values().stream()
                .mapToInt(i -> i).min().orElseThrow(RuntimeException::new);

        String matchingLayer = layersAndNumberOfZeros.keySet().stream()
                .filter(i -> layersAndNumberOfZeros.get(i) == leastNumbersOfZeros)
                .findFirst()
                .orElseThrow(RuntimeException::new);

        int numberoOfOnes = StringUtils.countMatches(matchingLayer, "1");
        int numberoOfTwos = StringUtils.countMatches(matchingLayer, "2");

        int result = numberoOfOnes * numberoOfTwos;
        return result;
    }

    public void part02() throws FileNotFoundException {
        String[] layers = getLayers().toArray(new String[]{});

        String finalImage = getFinalImage(layers);
        String[] imageInRows = finalImage.split("(?<=\\G.{" + pixelsWide + "})");
        for(String row : imageInRows) {
            System.out.println(row);
        }
    }

    private String getFinalImage(String[] layers) {
        StringBuilder finalImage = new StringBuilder();
        for (int pixelPosition = 0; pixelPosition < pixelsPerLayer; pixelPosition++) {
            finalImage.append(getPixelPosition(layers, pixelPosition, finalImage));
        }
        return finalImage.toString();
    }

    private String getPixelPosition(String[] layers, int pixelPosition, StringBuilder finalImage) {
        for (int layerIndex = 0; layerIndex < layers.length; layerIndex++) {
            String pixel = layers[layerIndex].substring(pixelPosition, pixelPosition + 1);
            if(Integer.parseInt(pixel) < 2) {
                return pixel;
            }
        }
        throw new RuntimeException("Did not find expected pixel");
    }

    private List<String> getLayers() throws FileNotFoundException {
        String input = fileUtil.getFileContentRow("day08input", 0);

        return Arrays.stream(input.split("(?<=\\G.{" + pixelsPerLayer + "})"))
                .collect(Collectors.toList());
    }
}
