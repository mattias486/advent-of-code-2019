package com.mattias.advent.y2019.day09;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day09 {


    FileUtil fileUtil = new FileUtil();
    long[] day09backup;
    long[] day09input;
    int relativePosition;

    public Day09() throws FileNotFoundException {
        day09backup = Arrays.stream(fileUtil.getFileContentRow("day09input", 0).split(","))
                .mapToLong(Long::parseLong)
                .toArray();
    }

    public List<Long> part01And02(int input) {
        List<Long> output = new ArrayList<>();
        day09input = new long[day09backup.length];
        System.arraycopy(day09backup, 0, day09input, 0, day09backup.length);
        relativePosition = 0;

        int position = 0;
        while(true) {
            long operatorAndParameterMode = day09input[position];
            int operator = getOperator(operatorAndParameterMode);
            long[] parameterModes = getParameterModes(operatorAndParameterMode);
            boolean jump = false;
            if(operator == 3 || operator == 4) {
                if(operator == 3) {
                    int parameter1Address = getParameterAddress(parameterModes[0], position + 1);
                    setValueAtIndex(parameter1Address, input);
                } else if (operator == 4) {
                    long parameter1 = getParameterValue(parameterModes[0], position + 1);
                    output.add(parameter1);
                }
            }
            else if(operator == 1 || operator == 2 || operator == 7 || operator == 8) {
                long parameter1 = getParameterValue(parameterModes[0], position+1);
                long parameter2 = getParameterValue(parameterModes[1], position+2);
                int parameter3 = getParameterAddress(parameterModes[2], position+3);
                if(operator == 1) {
                    setValueAtIndex(parameter3, parameter1 + parameter2);
                } else if(operator == 2) {
                    setValueAtIndex(parameter3, parameter1 * parameter2);
                } else if(operator == 7) {
                    //less then
                    setValueAtIndex(parameter3, (parameter1 < parameter2) ? 1 : 0);
                } else if(operator == 8) {
                    //equals
                    setValueAtIndex(parameter3, (parameter1 == parameter2) ? 1 : 0);
                }
            } else if(operator == 5 || operator == 6) {
                long parameter1 = getParameterValue(parameterModes[0], position+1);
                long parameter2 = getParameterValue(parameterModes[1], position+2);
                if(operator == 5) {
                    //jump if true
                    if(parameter1 != 0) {
                        position = (int)parameter2;
                        jump = true;
                    }
                } else if(operator == 6) {
                    //jump if false
                    if (parameter1 == 0) {
                        position = (int)parameter2;
                        jump = true;
                    }
                }
            } else if (operator == 9) {
                long parameter1 = getParameterValue(parameterModes[0], position+1);
                relativePosition += parameter1;
            }
            else {
                throw new RuntimeException("Wrong operator");
            }
            if(jump == false) {
                position = (position + getInstructionLength(operator)) % day09input.length;
            }
            if(day09input[position] == 99) {
                return output;
            }
        }
    }

    private long getParameterValue(long parameterMode, int position) {
        return getMemoryAtIndex(getParameterAddress(parameterMode, position));
    }

    private int getParameterAddress(long parameterMode, int position) {
        if (parameterMode == 0) {
            return (int)getMemoryAtIndex(position);
        } else if (parameterMode == 1) {
            return position;
        } else if (parameterMode == 2) {
            return relativePosition + (int)getMemoryAtIndex(position);
        }
        throw new RuntimeException("Unknown parameterMode");
    }

    private long getMemoryAtIndex(int index) {
        if(index < 0) {
            throw new RuntimeException("Accessing negative memory");
        }
        increaseMemoryIfRequired(index);
        return day09input[index];
    }

    private void setValueAtIndex(int index, long value) {
        increaseMemoryIfRequired(index);
        day09input[index] = value;
    }

    private void increaseMemoryIfRequired(int index) {
        if(day09input.length <= index) {
            long[] newInput = new long[index+1];
            System.arraycopy(day09input, 0, newInput, 0, day09input.length);
            day09input = newInput;
        }
    }

    private int getOperator(long operatorAndParameterMode) {
        return (int) (operatorAndParameterMode % 100);
    }

    private long[] getParameterModes(long operatorAndParameterMode) {
        return new long[] {
                operatorAndParameterMode % 1000 / 100,
                operatorAndParameterMode % 10000 / 1000,
                operatorAndParameterMode / 10000
        };
    }

    private int getInstructionLength(int operator) {
        switch (operator) {
            case 3:
            case 4:
            case 9:
                return 2;
            case 5:
            case 6:
                return 3;
            case 1:
            case 2:
            case 7:
            case 8:
               return 4;
        }
        throw new RuntimeException("Unhandled operation");
    }
}
