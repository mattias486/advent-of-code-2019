package com.mattias.advent.y2019.day05;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.Arrays;

public class Day05 {

    FileUtil fileUtil = new FileUtil();
    int[] day05Backup;

    public Day05() throws FileNotFoundException {
        day05Backup = Arrays.stream(fileUtil.getFileContentRow("day05input", 0).split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    public Integer part01And02(int input) {
        int[] day05input = new int[day05Backup.length];
        System.arraycopy(day05Backup, 0, day05input, 0, day05Backup.length);

        int position = 0;
        while(true) {
            int operatorAndParameterMode = day05input[position];
            int operator = getOperator(operatorAndParameterMode);
            int[] parameterModes = getParameterModes(operatorAndParameterMode);
            boolean jump = false;
            if(operator == 3 || operator == 4) {
                int parameter1 = parameterModes[0] == 0 ? day05input[position+1] : position+1;
                if(operator == 3) {
                    day05input[parameter1] = input;
                } else if (operator == 4) {
                    System.out.println("Op4:" + day05input[parameter1]);
                }

            }
            else if(operator == 1 || operator == 2 || operator == 7 || operator == 8) {
                int parameter1 = parameterModes[0] == 0 ? day05input[day05input[position+1]] : day05input[position+1];
                int parameter2 = parameterModes[1] == 0 ? day05input[day05input[position+2]] : day05input[position+2];
                int parameter3 = day05input[position+3]; //never in immediate mode
                if(operator == 1) {
                    day05input[parameter3] = parameter1 + parameter2;
                } else if(operator == 2) {
                    day05input[parameter3] = parameter1 * parameter2;
                } else if(operator == 7) {
                    //less then
                    day05input[parameter3] = (parameter1 < parameter2) ? 1 : 0;
                } else if(operator == 8) {
                    //equals
                    day05input[parameter3] = (parameter1 == parameter2) ? 1 : 0;
                }
            } else if(operator == 5 || operator == 6) {
                int parameter1 = parameterModes[0] == 0 ? day05input[day05input[position+1]] : day05input[position+1];
                int parameter2 = parameterModes[1] == 0 ? day05input[day05input[position+2]] : day05input[position+2];
                if(operator == 5) {
                    //jump if true
                    if(parameter1 != 0) {
                        position = parameter2;
                        jump = true;
                    }
                } else if(operator == 6) {
                    //jump if false
                    if (parameter1 == 0) {
                        position = parameter2;
                        jump = true;
                    }
                }
            }
            else {
                throw new RuntimeException("Wrong operator");
            }
            if(jump == false) {
                position = (position + getInstructionLength(operator)) % day05input.length;
            }
            if(day05input[position] == 99) {
                return day05input[0];
            }
        }
    }

    private int getOperator(int operatorAndParameterMode) {
        return operatorAndParameterMode % 100;
    }

    private int[] getParameterModes(int operatorAndParameterMode) {
        return new int[] {
                operatorAndParameterMode % 1000 / 100,
                operatorAndParameterMode % 10000 / 1000,
                operatorAndParameterMode / 10000
        };
    }

    private int getInstructionLength(int operator) {
        if(operator == 3 || operator == 4) {
            return 2;
        }
        if(operator == 5 || operator == 6) {
            return 3;
        }
        return 4;
    }
}
