package com.mattias.advent.y2019.day03;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class Day03 {

    FileUtil fileUtil = new FileUtil();

    public void part1And2() throws FileNotFoundException {

        List<String> day03input = fileUtil.getFileContentAsStringList("day03input");
        List<DirectionAndLength> firstCable = getCable(day03input.get(0));
        List<DirectionAndLength> secondCable = getCable(day03input.get(1));

        int x = 0;
        int y = 0;
        List<CablePosition> firstCablePositions = getCablePositions(firstCable, x, y);
        List<CablePosition> secondCablePositions = getCablePositions(secondCable, x, y);

        List<CablePosition> crossingSections = firstCablePositions.stream()
                .distinct()
                .filter(secondCablePositions::contains)
                .collect(Collectors.toList());

        System.out.println("Number of cross sections : " + crossingSections.size());

        int minDistance = crossingSections.stream()
                .mapToInt(i -> Math.abs(i.x) + Math.abs(i.y))
                .min()
                .orElseThrow(RuntimeException::new);

        System.out.println("Min distance : " + minDistance);

        int minTotalCableLength = crossingSections.stream()
                .distinct()
                .mapToInt(i -> firstCablePositions.indexOf(i) + 1 + secondCablePositions.indexOf(i) + 1)
                .min()
                .orElseThrow(RuntimeException::new);

        System.out.println("Min total cable length : " + minTotalCableLength);
    }

    private List<CablePosition> getCablePositions(List<DirectionAndLength> cableItem, int x, int y) {
        List<CablePosition> cablePositions = new ArrayList<>();
        for(DirectionAndLength firstCableItem : cableItem) {
            if(firstCableItem.direction == 'U') {
                for(int i=1; i <= firstCableItem.distance; i++) {
                    cablePositions.add(new CablePosition(x, y+i));
                }
                y += firstCableItem.distance;
            }
            else if(firstCableItem.direction == 'D') {
                for(int i=1; i <= firstCableItem.distance; i++) {
                    cablePositions.add(new CablePosition(x, y-i));
                }
                y -= firstCableItem.distance;
            }
            else if(firstCableItem.direction == 'R') {
                for(int i=1; i <= firstCableItem.distance; i++) {
                    cablePositions.add(new CablePosition(x+i, y));
                }
                x += firstCableItem.distance;
            }
            else if(firstCableItem.direction == 'L') {
                for(int i=1; i <= firstCableItem.distance; i++) {
                    cablePositions.add(new CablePosition(x-i, y ));
                }
                x -= firstCableItem.distance;
            }
        }
        return cablePositions;
    }

    private List<DirectionAndLength> getCable(String inputRow) {
        return Arrays.stream(inputRow.split(","))
                .map(this::fromString)
                .collect(Collectors.toList());
    }

    class CablePosition {
        public final int x;
        public final int y;

        public CablePosition(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "CablePosition{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            CablePosition o1 = (CablePosition) o;
            return o1.x == this.x && o1.y == this.y;
        }
    }

    class DirectionAndLength {
        private char direction;
        private int distance;

        public DirectionAndLength(char direction, int distance) {
            this.direction = direction;
            this.distance = distance;
        }
    }

    public DirectionAndLength fromString(String input) {
        return new DirectionAndLength(input.charAt(0), Integer.parseInt(input.substring(1)));
    }

}
