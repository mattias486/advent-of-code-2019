package com.mattias.advent.y2019.day01;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;

public class Day01 {
    public long part01() throws FileNotFoundException {
        return new FileUtil().getFileContentAsIntegerList("day01input")
                .stream()
                .mapToInt(i -> (i/3) - 2)
                .sum();
    }

    public long part02() throws FileNotFoundException {
        return new FileUtil().getFileContentAsIntegerList("day01input")
                .stream()
                .mapToInt(this::getFuelForModule)
                .sum();
    }

    public int getFuelForModule(int weight) {
        int fuelForWeight = (weight/3) - 2;
        return fuelForWeight < 1 ? 0 : fuelForWeight + getFuelForModule(fuelForWeight);
    }
}
