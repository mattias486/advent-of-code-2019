package com.mattias.advent.y2019.day06;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day06 {

    FileUtil fileUtil = new FileUtil();

    public int part01() throws FileNotFoundException {

        Map<String, String> planets = getPlanets();
        return planets.values().stream()
                .mapToInt(i -> getOrbitLength(i, planets, 0))
                .sum();
    }

    private Map<String, String> getPlanets() throws FileNotFoundException {
        return fileUtil.getFileContentAsStringList("day06input").stream()
                    .collect(Collectors.toMap(i -> i.split("\\)")[1],
                            i -> i.split("\\)")[0]));
    }

    private int getOrbitLength(String planet, Map<String, String> planets, int currentLength) {
        if(!planets.containsValue(planet)) {
            return currentLength;
        }
        return getOrbitLength(planets.get(planet), planets, 1 + currentLength);
    }

    public int part02() throws FileNotFoundException {
        Map<String, String> planets = getPlanets();
        List<String> youParentPlanets = getListOfParentPlanets("YOU", planets, new ArrayList<>());
        List<String> sanParentPlanets = getListOfParentPlanets("SAN", planets, new ArrayList<>());
        String sharedParent = youParentPlanets.stream()
                .filter(sanParentPlanets::contains)
                .findFirst()
                .orElseThrow(RuntimeException::new);

        //-1, since one parent in the you list your already in orbit of, the sharedplanet is present in both lists
        int travelDistance = youParentPlanets.indexOf(sharedParent) + sanParentPlanets.indexOf(sharedParent) - 2;
        return travelDistance;
    }

    private List<String> getListOfParentPlanets(String planet, Map<String, String> planets, List<String> result) {
        if(planets.containsKey(planet)) {
            result.add(planet);
            return getListOfParentPlanets(planets.get(planet), planets, result);
        }
        return result;
    }
}
