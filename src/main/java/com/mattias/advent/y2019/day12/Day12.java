package com.mattias.advent.y2019.day12;

import com.mattias.advent.y2019.common.FileUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

public class Day12 {

    FileUtil fileUtil = new FileUtil();

    @Data
    @AllArgsConstructor
    class Planet {
        Location location;
        Velocity velocity;
    }

    @Data
    class Location {
        final int x;
        final int y;
        final int z;
    }

    @Data
    class Velocity {
        final int x;
        final int y;
        final int z;
    }

    public void part01() throws FileNotFoundException {
        List<Planet> planets = getPlanets();
        for (int i = 0; i < 1_000; i++) {
            //System.out.println("Round " + i);
            movePlanetsOneTurn(planets);
        }
        System.out.println("Total energy : " + getTotalEnergyOfPlanets(planets));
    }

    private int getTotalEnergyOfPlanets(List<Planet> planets) {
        return planets.stream()
                .mapToInt(this::getTotalPlanetEnergy)
                .sum();
    }

    private void movePlanetsOneTurn(List<Planet> planets) {
        for (Planet planetItem : planets) {
            //System.out.println(planetItem);
            Velocity gravity = calculateGravity(planetItem, planets);
            planetItem.velocity = addVelocities(planetItem.velocity, gravity);
        }
        for (Planet planetItem : planets) {
            addVelocityToLocation(planetItem);
        }
    }

    private int getTotalPlanetEnergy(Planet planet) {
        return getPotentialEnergy(planet) * getKineticEnergy(planet);
    }

    private int getPotentialEnergy(Planet planet) {
        Location location = planet.location;
        return Math.abs(location.x) + Math.abs(location.y) + Math.abs(location.z);
    }

    private int getKineticEnergy(Planet planet) {
        Velocity velocity = planet.velocity;
        return Math.abs(velocity.x) + Math.abs(velocity.y) + Math.abs(velocity.z);
    }

    private void addVelocityToLocation(Planet planetItem) {
        planetItem.location = new Location(planetItem.location.x + planetItem.velocity.x,
                planetItem.location.y + planetItem.velocity.y,
                planetItem.location.z + planetItem.velocity.z);
    }

    private Velocity calculateGravity(Planet mainPlanet, List<Planet> planets) {
        return planets.stream()
                .filter(planet -> planet != mainPlanet)
                .map(otherPlanet -> new Velocity(getVelocityAxis(mainPlanet.location.x, otherPlanet.location.x),
                        getVelocityAxis(mainPlanet.location.y, otherPlanet.location.y),
                        getVelocityAxis(mainPlanet.location.z, otherPlanet.location.z)
                ))
                .reduce(this::addVelocities)
                .orElseThrow(RuntimeException::new);
    }

    private Velocity addVelocities(Velocity v1, Velocity v2) {
        return new Velocity(v1.x + v2.x,
                v1.y + v2.y,
                v1.z + v2.z);
    }

    public int getVelocityAxis(int p1, int p2) {
        if (p1 == p2) {
            return 0;
        }
        return p1 < p2 ? 1 : -1;
    }

    private List<Planet> getPlanets() throws FileNotFoundException {
        List<String> day12input = fileUtil.getFileContentAsStringList("day12input");
        return day12input.stream()
                .map(row -> row.replace(">", ""))
                .map(row -> row.replace("<", ""))
                .map(row -> {
                    String[] parts = row.split(",");
                    return new Planet(getLocation(parts),
                            new Velocity(0, 0, 0));
                })
                .collect(Collectors.toList());
    }

    private Location getLocation(String[] parts) {
        return new Location(Integer.parseInt(parts[0].substring(2)),
                Integer.parseInt(parts[1].substring(3)),
                Integer.parseInt(parts[2].substring(3)));
    }

    @Data
    private class PlanetsState {
        final Planet p1;
        final Planet p2;
        final Planet p3;
        final Planet p4;
    }

    public void part02() throws FileNotFoundException {
        List<Planet> planets = getPlanets();
        //System.out.println("Number of turns before returning to previous state : " + previousStates.size());
    }

    private PlanetsState getPlanetState(List<Planet> planets) {
        return new PlanetsState(planets.get(0),
                planets.get(1),
                planets.get(2),
                planets.get(3));
    }
}
