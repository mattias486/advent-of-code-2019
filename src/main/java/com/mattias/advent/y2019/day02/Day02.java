package com.mattias.advent.y2019.day02;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Day02 {

    FileUtil fileUtil = new FileUtil();
    int[] day02Backup;

    public Day02() throws FileNotFoundException {
        day02Backup = Arrays.stream(fileUtil.getFileContentRow("day02input", 0).split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    public Integer part01(int valueOnIndex1, int valueOnIndex2) {
        int[] day02input = new int[day02Backup.length];
        System.arraycopy(day02Backup, 0, day02input, 0, day02Backup.length);

        day02input[1] = valueOnIndex1;
        day02input[2] = valueOnIndex2;

        int position = 0;
        while(true) {
            int operator = day02input[position];
            if(operator == 1) {
                day02input[day02input[position+3]] = day02input[day02input[position+2]] + day02input[day02input[position+1]];
            } else if(operator == 2) {
                day02input[day02input[position+3]] = day02input[day02input[position+2]] * day02input[day02input[position+1]];
            } else {
                throw new RuntimeException("Wrong operator");
            }
            position = (position + 4) % day02input.length;
            if(day02input[position] == 99) {
                return day02input[0];
            }
        }
    }

    private class NounAndVerb {
        private int noun;
        private int verb;

        public NounAndVerb(int noun, int verb) {
            this.noun = noun;
            this.verb = verb;
        }

        public int getNoun() {
            return noun;
        }

        public int getVerb() {
            return verb;
        }
    }

    public void part02() {
        int wantedResult = 19690720;

        NounAndVerb wantedNounAndVerb = IntStream.range(0, 100)
                .boxed()
                .flatMap(noun -> IntStream.range(0, 100)
                        .mapToObj(verb -> new NounAndVerb(noun, verb)))
                .parallel()
                .filter(nounAndVerb -> wantedResult == part01(nounAndVerb.getNoun(), nounAndVerb.getVerb()))
                .findAny()
                .orElseThrow(RuntimeException::new);

        System.out.println("Found noun : " + wantedNounAndVerb.getNoun() + " and verb : " + wantedNounAndVerb.getVerb());
        System.out.println("Resulting in : " + (100 * wantedNounAndVerb.getNoun() + wantedNounAndVerb.getVerb()));
    }
}
