package com.mattias.advent.y2019.day22;

import com.mattias.advent.y2019.common.FileUtil;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day22 {

    FileUtil fileUtil = new FileUtil();

    public int[] getDeck(int maxSize) {
        return IntStream.range(0, maxSize)
                .toArray();
    }

    public List<String> getInput() throws FileNotFoundException {
        return fileUtil.getFileContentAsStringList("day22input");
    }

    public List<Shuffle> getShuffles(List<String> input) {
        return input.stream()
                .map(this::toShuffle)
                .collect(Collectors.toList());
    }

    private Shuffle toShuffle(String s) {
        if("deal into new stack".equals(s)) {
            return new Shuffle(ShuffleType.NEW_STACK, 0);
        }
        if(s.startsWith("deal with increment")) {
            String[] s1 = s.split(" ");
            return new Shuffle(ShuffleType.DEAL_WITH_INCREMENT, Integer.parseInt(s1[s1.length -1]));
        }
        if(s.startsWith("cut")) {
            String[] s1 = s.split(" ");
            return new Shuffle(ShuffleType.CUT, Integer.parseInt(s1[s1.length -1]));
        }
        throw new RuntimeException("Invalid shuffle");
    }

    public void part01() throws FileNotFoundException {
        List<Shuffle> shuffles = getShuffles(getInput());
        int[] deck = IntStream.range(0, 10_007).toArray();
        int[] result = doShuffles(deck, shuffles);

        List<Integer> resultAsList = IntStream.of(result).boxed().collect(Collectors.toList());
        int indexOf2019 = resultAsList.indexOf(2019);
        System.out.println("Index of card 2019 after shuffles : " + indexOf2019);
    }

    public int[] doShuffles(int[] deck, List<Shuffle> shuffles) {
      int[] result = deck;
      for(Shuffle shuffleItem : shuffles) {
          result = doShuffle(result, shuffleItem);
      }
      return result;
    }

    private int[] doShuffle(int[] deck, Shuffle shuffleItem) {
        if(ShuffleType.NEW_STACK == shuffleItem.type) {
            return dealIntoNewStack(deck);
        }
        if (ShuffleType.CUT == shuffleItem.type) {
            return cutCards(deck, shuffleItem.argument);
        }
        if (ShuffleType.DEAL_WITH_INCREMENT == shuffleItem.type) {
            return dealWithIncrement(deck, shuffleItem.argument);
        }
        throw new RuntimeException("Can not shuffle type");
    }

    protected int[] dealIntoNewStack(int[] source) {
        return IntStream.range(0, source.length)
                .map(i -> source[source.length - 1 - i])
                .toArray();
    }

    protected int[] cutCards(int[] source, int cut) {
        int absCut = Math.abs(cut);
        if(cut < 0) {
            IntStream sourceStream = Arrays.stream(source, 0, source.length - absCut);
            IntStream cutStream = Arrays.stream(source, source.length - absCut, source.length);
            return IntStream.concat(cutStream, sourceStream)
                    .toArray();
        }
        IntStream sourceStream = Arrays.stream(source, absCut, source.length);
        IntStream cutStream = Arrays.stream(source, 0, absCut);
        return IntStream.concat(sourceStream, cutStream)
                .toArray();
    }

    protected int[] dealWithIncrement(int[] source, int increment) {
        int[] result = new int[source.length];
        for (int i = 0; i < source.length; i++) {
            result[(i * increment) % source.length] = source[i];
        }
        return result;
    }

    public void part02() {
        
    }
}
