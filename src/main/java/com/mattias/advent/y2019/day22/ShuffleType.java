package com.mattias.advent.y2019.day22;

public enum ShuffleType {
    NEW_STACK, DEAL_WITH_INCREMENT, CUT
}
