package com.mattias.advent.y2019.day22;

import lombok.Data;

@Data
public class Shuffle {
    final ShuffleType type;
    final int argument;
}
