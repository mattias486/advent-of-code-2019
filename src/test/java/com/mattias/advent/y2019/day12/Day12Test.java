package com.mattias.advent.y2019.day12;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day12Test {

    Day12 day12 = new Day12();

    @Test
    public void testPart01() throws FileNotFoundException {
        day12.part01();
    }

    @Test
    public void testPart02() throws FileNotFoundException {
        day12.part02();
    }
}
