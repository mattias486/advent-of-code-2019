package com.mattias.advent.y2019.day10;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

public class Day10Test {

    public Day10 day10 = new Day10();

    @Test
    public void testPart01() throws FileNotFoundException {
        day10.part01();
    }

    @Test
    public void testIsPlanetReachable() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(12, 40);
        Set<Day10.Planet> planets = new HashSet<>();

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsPlanetNotReachable() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(12, 40);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(6, 20));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetReachable2() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(40, 12);
        Set<Day10.Planet> planets = new HashSet<>();

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsPlanetNotReachable2() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(40, 12);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(20, 6));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }


    @Test
    public void testIsPlanetReachable3() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(-40, 12);
        Set<Day10.Planet> planets = new HashSet<>();

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsPlanetNotReachable3() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(-40, 12);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(-20, 6));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetReachable4() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(-40, -12);
        Set<Day10.Planet> planets = new HashSet<>();

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsPlanetNotReachable4() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(-40, -12);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(-20, -6));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetReachable5() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(40, -12);
        Set<Day10.Planet> planets = new HashSet<>();

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsPlanetNotReachable5() {
        Day10.Planet planet = new Day10.Planet(0, 0);
        Day10.Planet planetToReach = new Day10.Planet(40, -12);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(20, -6));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetNotReachable6() {
        Day10.Planet planet = new Day10.Planet(4, 0);
        Day10.Planet planetToReach = new Day10.Planet(4, 3);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(4, 2));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetNotReachable7() {
        Day10.Planet planet = new Day10.Planet(-4, 0);
        Day10.Planet planetToReach = new Day10.Planet(-4, 3);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(-4, 2));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetNotReachable8() {
        Day10.Planet planet = new Day10.Planet(4, 0);
        Day10.Planet planetToReach = new Day10.Planet(4, -3);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(4, -2));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsPlanetNotReachable9() {
        Day10.Planet planet = new Day10.Planet(-4, 0);
        Day10.Planet planetToReach = new Day10.Planet(-4, -3);
        Set<Day10.Planet> planets = new HashSet<>();
        planets.add(new Day10.Planet(-4, -2));

        boolean result = day10.isPlanetReachable(planet, planetToReach, planets);

        Assert.assertFalse(result);
    }
}
