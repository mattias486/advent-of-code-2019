package com.mattias.advent.y2019.day04;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Day04Test {

    private Day04 day04 = new Day04();

    @Test
    public void testPart01() {
        long result = day04.part01();
        System.out.println("Result from part01And02 : " + result);
    }

    @Test
    public void testPart02() {
        long result = day04.part02();
        System.out.println("Result from part02 : " + result);
    }

    @Test
    public void testTwoAdjacentDigitsAreTheSame() {
        int[] valids = {111111, 1156789, 233456, 234456, 234556, 234566};
        int[] invalids = {123789};

        Arrays.stream(valids)
                .forEach(i -> Assert.assertTrue(day04.twoAdjacentDigitsAreTheSame(i)));
        Arrays.stream(invalids)
                .forEach(i -> Assert.assertFalse(day04.twoAdjacentDigitsAreTheSame(i)));
    }

    @Test
    public void testDigitsNeverDecrease() {
        int[] valids = {111111, 123456};
        int[] invalids = {223450, 214567, 221456, 222156, 222216};

        Arrays.stream(valids)
                .forEach(i -> Assert.assertTrue(day04.digitsNeverDecrease(i)));
        Arrays.stream(invalids)
                .forEach(i -> Assert.assertFalse(day04.digitsNeverDecrease(i)));
    }

    @Test
    public void testMatchingDigitsIsNotPartOfLargerDigitGroup() {
        int[] valids = {112233, 111122};
        int[] invalids = {123444};

        Arrays.stream(valids)
                .forEach(i -> Assert.assertTrue(day04.matchingDigitsIsNotPartOfLargerDigitGroup(i)));
        Arrays.stream(invalids)
                .forEach(i -> Assert.assertFalse(day04.matchingDigitsIsNotPartOfLargerDigitGroup(i)));
    }

}
