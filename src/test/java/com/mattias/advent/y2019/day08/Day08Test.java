package com.mattias.advent.y2019.day08;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day08Test {

    Day08 day08 = new Day08();

    @Test
    public void testPart01() throws FileNotFoundException {
        int result = day08.part01();
        System.out.println("Result part01 : " + result);
    }

    @Test
    public void testPart02() throws FileNotFoundException {
        day08.part02();
    }

}
