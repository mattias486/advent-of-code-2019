package com.mattias.advent.y2019.day22;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.stream.IntStream;

public class Day22Test {

    Day22 day22 = new Day22();

    @Test
    public void testDealIntoNewStack() {
        int[] deck = IntStream.range(0, 10).toArray();
        int[] ints = day22.dealIntoNewStack(deck);
        Assert.assertEquals(9, ints[0]);
        Assert.assertEquals(0, ints[9]);
    }

    @Test
    public void testCutWithPositive() {
        int[] deck = IntStream.range(0, 10).toArray();
        int[] ints = day22.cutCards(deck, 3);
        Assert.assertEquals(2, ints[9]);
        Assert.assertEquals(0, ints[7]);
        Assert.assertEquals(3, ints[0]);
        Assert.assertEquals(9, ints[6]);
    }

    @Test
    public void testCutWithNegative() {
        int[] deck = IntStream.range(0, 10).toArray();
        int[] ints = day22.cutCards(deck, -4);
        Assert.assertEquals(6, ints[0]);
        Assert.assertEquals(9, ints[3]);
        Assert.assertEquals(0, ints[4]);
        Assert.assertEquals(5, ints[9]);
    }

    @Test
    public void testDealWithIncrement() {
        int[] deck = IntStream.range(0, 10).toArray();
        int[] ints = day22.dealWithIncrement(deck, 3);
        Assert.assertEquals(0, ints[0]);
        Assert.assertEquals(7, ints[1]);
        Assert.assertEquals(4, ints[2]);
        Assert.assertEquals(1, ints[3]);
        Assert.assertEquals(3, ints[9]);
    }

    @Test
    public void testPart01() throws FileNotFoundException {
        day22.part01();
    }

    @Test
    public void testPart02() {
        day22.part02();
    }
}
