package com.mattias.advent.y2019.day05;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day05Test {

    private Day05 day05 = new Day05();

    public Day05Test() throws FileNotFoundException {
    }

    @Test
    public void testPart01() {
        day05.part01And02(1);
    }

    @Test
    public void testPart02() {
        day05.part01And02(5);
    }
}
