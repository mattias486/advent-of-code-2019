package com.mattias.advent.y2019.day03;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day03Test {

    public Day03 day03 = new Day03();

    @Test
    public void testPart1AndTwo() throws FileNotFoundException {
        day03.part1And2();
    }
}
