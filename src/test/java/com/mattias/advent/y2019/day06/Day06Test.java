package com.mattias.advent.y2019.day06;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day06Test {

    Day06 day06 = new Day06();

    @Test
    public void testPart01() throws FileNotFoundException {
        int result = day06.part01();

        System.out.println("Part01 : " + result);
    }

    @Test
    public void testPart02() throws FileNotFoundException {
        int result = day06.part02();

        System.out.println("Part02 : " + result);
    }
}
