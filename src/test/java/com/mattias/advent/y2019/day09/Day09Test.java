package com.mattias.advent.y2019.day09;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.List;

public class Day09Test {

    Day09 day09 = new Day09();

    public Day09Test() throws FileNotFoundException {
    }

    @Test
    public void testPart01() {
        List<Long> result = day09.part01And02(1);
        System.out.println(result);
    }

    @Test
    public void testPart02() {
        List<Long> result = day09.part01And02(2);
        System.out.println(result);
    }

}
