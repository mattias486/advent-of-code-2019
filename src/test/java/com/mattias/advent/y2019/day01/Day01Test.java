package com.mattias.advent.y2019.day01;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;

public class Day01Test {

    Day01 day01 = new Day01();

    @Test
    public void testPart01() throws FileNotFoundException {
        long result = day01.part01();
        //System.out.println(result);
    }

    @Test
    public void testFuelForModule() {
        Assert.assertEquals(966, day01.getFuelForModule(1969));
        Assert.assertEquals(50346, day01.getFuelForModule(100756));
    }

    @Test
    public void testPart02() throws FileNotFoundException {
        long result = day01.part02();
        //System.out.println(result);
    }
}
