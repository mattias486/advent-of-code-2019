package com.mattias.advent.y2019.day02;

import org.junit.Test;

import java.io.FileNotFoundException;

public class Day02Test {

    private Day02 day02 = new Day02();

    public Day02Test() throws FileNotFoundException {
    }

    @Test
    public void testPart01() {
        Integer firstInteger = day02.part01(12, 2);
        System.out.println("Found operator 99, exit state of pos 0 : " + firstInteger);
    }

    @Test
    public void testPart02() {
        day02.part02();
    }
}
