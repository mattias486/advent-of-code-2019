#!/bin/bash
day=$1
if [ -z $day ]; then
  echo "Missing day parmeter".
fi

day_java_template=template/DayX.java
day_java_test_template=template/DayXTest.java
src_dir=./src/main/java/com/mattias/advent/y2019/day${day}
test_dir=./src/test/java/com/mattias/advent/y2019/day${day}
resource_dir=./src/main/resources
mkdir -p ${src_dir}
mkdir -p ${test_dir}


touch ${resource_dir}/day${day}input
cat ${day_java_template} | sed "s/DayX/Day${day}/g;s/dayX/day${day}/g" > ${src_dir}/Day${day}.java
cat ${day_java_test_template} | sed "s/DayX/Day${day}/g;s/dayX/day${day}/g" > ${test_dir}/Day${day}Test.java

echo "Good luck with day${day} !"
